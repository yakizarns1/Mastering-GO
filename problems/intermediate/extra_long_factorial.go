package intermediate

import (
	"math/big"
)

func extraLongFactorials(n int32) *big.Int {
	var sum *big.Int
	if n == 0 {
		return big.NewInt(1)
	}

	for i := int64(1); i <= int64(n); i++ {
		if i == 1 {
			sum = big.NewInt(i)
		}
		sum.Mul(sum, big.NewInt(i))
	}

	return sum
}
