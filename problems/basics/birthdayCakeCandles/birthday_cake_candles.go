package birthdayCakeCandles

func birthdayCakeCandles(candles []int32) int32 {
	var sum int32
	var currentValue int32
	for item := range candles {
		if currentValue < candles[item] {
			currentValue = candles[item]
			sum = 1
		} else if currentValue == candles[item] {
			sum++
		}
	}

	return sum
}
