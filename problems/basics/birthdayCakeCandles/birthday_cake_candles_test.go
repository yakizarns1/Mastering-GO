package birthdayCakeCandles

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBirthdayCakeCandles(t *testing.T) {
	tt := []struct {
		candles  []int32
		expected int32
	}{
		{candles: []int32{3, 2, 1, 3}, expected: int32(2)},
		{candles: []int32{10, 90, 90, 13, 90, 75, 90, 8, 90, 43}, expected: int32(5)},
	}

	for _, tc := range tt {
		actual := birthdayCakeCandles(tc.candles)
		assert.Equal(t, tc.expected, actual)
	}

}
