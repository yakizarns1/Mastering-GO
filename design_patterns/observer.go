package main

import (
	"fmt"
	"log"
)

type Temperature struct {
	observers []chan float64
}

func (t *Temperature) subscribe(c chan float64) {
	t.observers = append(t.observers, c)
}

func (t *Temperature) update(temp float64) {
	for _, obs := range t.observers {
		go func(c chan float64) {
			log.Printf("temperature update %v", temp)
			c <- temp
		}(obs)
	}
}

func main() {
	ch := make(chan float64)
	ch1 := make(chan float64)
	ch2 := make(chan float64)
	ch3 := make(chan float64)

	temp := Temperature{}
	temp.subscribe(ch)
	temp.subscribe(ch1)
	temp.subscribe(ch2)
	temp.subscribe(ch3)

	temp.update(5.5)
	fmt.Printf("ch: %v, ch1: %v, ch2: %v, ch3 %v", <-ch, <-ch1, <-ch2, <-ch3)
}
