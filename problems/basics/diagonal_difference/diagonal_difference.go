package main

import (
	"fmt"
	"math"
)

func diagonalDifference(arr [][]int32) int32 {
	//var firstDiagonal int32
	//var secondDiagonal int32
	//dimensions := int(arr[0][0])
	//
	//for i := 1; i <= dimensions; i++ {
	//	for j := 0; j < dimensions; j++ {
	//		if i == j+1 {
	//			firstDiagonal = firstDiagonal + arr[i][j]
	//		}
	//		if (i-1)+j == dimensions-1 {
	//			secondDiagonal = secondDiagonal + arr[i][j]
	//		}
	//	}
	//}
	//if firstDiagonal != 0 && firstDiagonal < 0 {
	//	firstDiagonal = firstDiagonal * (-1)
	//}
	//if secondDiagonal != 0 && secondDiagonal < 0 {
	//	secondDiagonal = secondDiagonal * (-1)
	//}
	//expected := firstDiagonal - secondDiagonal
	//if expected != 0 && expected < 0 {
	//	expected = expected * (-1)
	//}
	// The above produces the right  output but the hackerank complains about runtime error

	var firstDiagonal int32
	var secondDiagonal int32

	for i := range arr {
		for j := range arr {
			if i == j {
				firstDiagonal = firstDiagonal + arr[i][j]
			}
			if i == len(arr)-j-1 {
				secondDiagonal = secondDiagonal + arr[i][j]
			}
		}
	}

	return int32(math.Abs(float64(firstDiagonal - secondDiagonal)))
}
func main() {
	test_arr := [][]int32{
		{3},
		{11, 2, 4},
		{4, 5, 6},
		{10, 8, -12},
	}

	difference := diagonalDifference(test_arr)
	fmt.Println(difference)
}
