## Sorting Algorithms:
- [ ] Bubble Sort (Time: O(n^2), Space: O(1))
- [ ] Merge Sort (Time: O(n log n), Space: O(n))
- [ ] Quick Sort (Time: O(n log n) average, O(n^2) worst, Space: O(log n))
- [ ] Selection Sort (Time: O(n^2), Space: O(1))
- [ ] Insertion Sort (Time: O(n^2), Space: O(1))
- [ ] Heap Sort (Time: O(n log n), Space: O(1))

## Searching Algorithms:
- [ ] Binary Search (Time: O(log n), Space: O(1))
- [ ] Hashing (Time: O(1), Space: O(n))

## Graph Algorithms:
- [ ] Depth-First Search (DFS) (Time: O(V + E), Space: O(V))
- [ ] Breadth-First Search (BFS) (Time: O(V + E), Space: O(V))
- [ ] Dijkstra's Algorithm (Time: O(V^2) or O(V + E) with heap, Space: O(V))
- [ ] Bellman-Ford Algorithm (Time: O(V*E), Space: O(V))
- [ ] Floyd-Warshall Algorithm (Time: O(V^3), Space: O(V^2))
- [ ] Minimum Spanning Tree (e.g., Prim's or Kruskal's algorithm) (Time: O(E log E) or O(E log V), Space: O(V))

## Dynamic Programming Algorithms:
- [ ] Dynamic Programming (Time: Varies, Space: Varies)
- [ ] Knapsack Problem (Time: O(nW), Space: O(nW))

## Greedy Algorithms:
- [ ] Greedy Algorithms (Time: Varies, Space: Varies)

## Data Structures:
- [ ] Binary Trees (e.g., BST, AVL, Red-Black) (Time: Varies, Space: Varies)
- [ ] Graph Theory (e.g., Graph representation, traversal) (Time: Varies, Space: Varies)
- [ ] Trie (Time: Varies, Space: Varies)
- [ ] Binary Search Tree (BST) (Time: Varies, Space: Varies)
- [ ] Union-Find Algorithm (Time: Varies, Space: Varies)
- [ ] Disjoint Set Union (DSU) (Time: Varies, Space: Varies)
- [ ] String algorithms (e.g., pattern matching, string manipulation) (Time: Varies, Space: Varies)
- [ ] Bit manipulation techniques (Time: Varies, Space: Varies)

## Other Algorithms:
- [ ] Sliding Window Technique (Time: Varies, Space: Varies)
- [ ] Two-pointer Technique (Time: Varies, Space: Varies)
- [ ] Divide and Conquer (Time: Varies, Space: Varies)
- [ ] Recursion (Time: Varies, Space: Varies)
- [ ] Big-O notation and time complexity analysis (Time: Varies, Space: Varies)
