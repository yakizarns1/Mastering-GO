package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDiagonalDifference(t *testing.T) {
	tt := []struct {
		arr      [][]int32
		expected int32
	}{
		{arr: [][]int32{
			{11, 2, 4},
			{4, 5, 6},
			{10, 8, -12},
		},
			expected: int32(15),
		},
		{arr: [][]int32{
			{5, 3, 8},
			{5, 9, 1},
			{7, 3, 3},
		},
			expected: int32(7),
		},
	}

	for _, tc := range tt {
		actual := diagonalDifference(tc.arr)
		assert.Equal(t, tc.expected, actual)
	}
}
