package merge_2_ascending_arrays

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestMerge(t *testing.T) {
	tt := []struct {
		name        string
		arr1        []int
		m           int
		arr2        []int
		n           int
		expectedArr []int
	}{
		{
			"len 3 array sorted",
			[]int{1, 2, 3, 0, 0, 0},
			3,
			[]int{2, 5, 6},
			3,
			[]int{1, 2, 2, 3, 5, 6},
		},
		{
			"arr2 0 elements",
			[]int{1},
			1,
			[]int{},
			0,
			[]int{1},
		},
		{
			"arr1 0 elements",
			[]int{0},
			0,
			[]int{1},
			1,
			[]int{1},
		},
	}

	for _, tc := range tt {
		actualArray := merge(tc.arr1, tc.m, tc.arr2, tc.n)
		assert.Equal(t, tc.expectedArr, actualArray)
	}
}
